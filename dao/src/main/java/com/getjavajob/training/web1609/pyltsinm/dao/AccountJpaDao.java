package com.getjavajob.training.web1609.pyltsinm.dao;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by Pyltsin on 12.03.2017. Algo8
 */
@Repository("accountDAO")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional(readOnly = true)

public class AccountJpaDao extends AbstractJpaDao<Account> {

    protected AccountJpaDao() {
        super(Account.class);
    }

    @Override
    protected CriteriaQuery<Account> findForQuery(CriteriaBuilder builder, String text) {
        CriteriaQuery<Account> criteriaQuery = builder.createQuery(Account.class);
        Root<Account> from = criteriaQuery.from(Account.class);
        criteriaQuery.select(from);
        criteriaQuery.where(builder.or(builder.like(from.get("firstName"), text), builder.like(from.get("middleName"), text),
                builder.like(from.get("lastName"), text)));

        criteriaQuery.orderBy(builder.asc(from.get("id")));

        return criteriaQuery;
    }

    @Override
    public String getName() {
        return "login";
    }
}
