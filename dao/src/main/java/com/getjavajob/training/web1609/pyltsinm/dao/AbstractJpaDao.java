package com.getjavajob.training.web1609.pyltsinm.dao;

import com.getjavajob.training.web1609.pyltsinm.common.interfaces.Identified;
import com.getjavajob.training.web1609.pyltsinm.common.interfaces.Pictured;
import com.getjavajob.training.web1609.pyltsinm.dao.interfaces.GenericDAO;
import com.getjavajob.training.web1609.pyltsinm.exception.PersistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Pyltsin on 12.03.2017. Algo8
 */

@Repository("AbstractDAOHibernate")
public abstract class AbstractJpaDao<T extends Identified> implements GenericDAO<T> {

    @PersistenceContext
    private EntityManager em;

    private Class<T> clazz;

    @Autowired
    protected AbstractJpaDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Transactional
    @Override
    public List<T> finderEntity(String textFind, Integer start, Integer len) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = findForQuery(criteriaBuilder, textFind);
        TypedQuery<T> tTypedQuery;
        if (start != null && len != null) {
            tTypedQuery = em.createQuery(criteriaQuery).setFirstResult(start).setMaxResults(len);
        } else {
            tTypedQuery = em.createQuery(criteriaQuery);
        }
        return tTypedQuery.getResultList();
    }

    @Override
    public T getByName(String login) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> from = criteriaQuery.from(clazz);
        criteriaQuery.select(from).where(criteriaBuilder.equal(from.get(getName()), login));
        TypedQuery<T> tTypedQuery = em.createQuery(criteriaQuery);
        T out;
        try {
            out = tTypedQuery.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return out;
    }

    @Transactional
    @Override
    public List<T> finderEntity(String textFind, Integer len) {
        return finderEntity(textFind, 0, len);
    }

    protected abstract CriteriaQuery<T> findForQuery(CriteriaBuilder criteriaQuery, String text);

    void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public void addBlob(T object, InputStream file) throws IOException {
        T obj = getById(object.getId());
        if (obj instanceof Pictured) {
            byte[] cache = new byte[file.available()];
            file.read(cache);
            ((Pictured) obj).setPicture(cache);
            em.merge(obj);
        }
    }

    @Override
    public byte[] getBlob(int id) throws PersistException {
        T obj = getById(id);
        if (obj instanceof Pictured) {
            return ((Pictured) obj).getPicture();
        }
        return null;
    }

    @Override
    public T persist(T object) {
        em.persist(object);
        return object;
    }

    @Override
    public void update(T object) {
        em.merge(object);
    }

    @Override
    public void delete(T object) throws PersistException {
        T obj = getById(object.getId());
        em.remove(obj);
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> from = criteriaQuery.from(clazz);
        criteriaQuery.select(from);
        TypedQuery<T> tTypedQuery = em.createQuery(criteriaQuery);
        return tTypedQuery.getResultList();
    }

    @Override
    public T getById(int key) {
        return em.find(clazz, key);
    }

    public abstract String getName();
}
