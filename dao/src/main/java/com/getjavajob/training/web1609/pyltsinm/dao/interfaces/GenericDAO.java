package com.getjavajob.training.web1609.pyltsinm.dao.interfaces;

import com.getjavajob.training.web1609.pyltsinm.common.interfaces.Identified;
import com.getjavajob.training.web1609.pyltsinm.exception.PersistException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Pyltsin on 05.12.2016. Algo8
 */
public interface GenericDAO<T extends Identified> {

    T getById(int key);

    List<T> getAll();

    T persist(T object);

    void update(T object);

    void delete(T object) throws PersistException;

    void addBlob(T object, InputStream file) throws IOException;

    byte[] getBlob(int id) throws PersistException;

    List<T> finderEntity(String textFind, Integer by);

    List<T> finderEntity(String textFind, Integer by, Integer from);

    T getByName(String name);
}
