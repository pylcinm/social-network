package com.getjavajob.training.web1609.pyltsinm.dao;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.common.Send;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Pyltsin on 13.08.2017.
 */

//todo add messege->15 period get

//todo add combat->16

@Repository
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)

public class SendDAO extends AbstractJpaDao<Send> {
    @PersistenceContext
    private EntityManager em;

    protected SendDAO() {
        super(Send.class);
    }

    @Override
    protected CriteriaQuery<Send> findForQuery(CriteriaBuilder criteriaQuery, String text) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return "";
    }

    public List<Send> getSends(Account accountFrom, Account accountTo, int len) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Send> criteriaQuery = builder.createQuery(Send.class);
        Root<Send> from = criteriaQuery.from(Send.class);
        criteriaQuery.select(from);
        criteriaQuery.where(builder.or(
                (builder.and(builder.equal(from.get("from"), accountFrom),
                        builder.equal(from.get("to"), accountTo))),
                builder.and(builder.equal(from.get("from"), accountTo),
                        builder.equal(from.get("to"), accountFrom))));

        criteriaQuery.orderBy(builder.desc(from.get("localDateTime")));
        TypedQuery<Send> tTypedQuery = em.createQuery(criteriaQuery).setMaxResults(len);
        return tTypedQuery.getResultList();
    }
}
