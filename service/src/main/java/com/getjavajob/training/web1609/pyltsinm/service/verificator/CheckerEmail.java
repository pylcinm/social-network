package com.getjavajob.training.web1609.pyltsinm.service.verificator;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.service.exception.VerificationException;

/**
 * Created by Pyltsin on 21.12.2016. Algo8
 */
public class CheckerEmail implements AbstractChecker {
    public void check(Account account) throws VerificationException {
        if (!account.getEmail().matches("[0-9a-zA-Z._%-]+@[0-9a-zA-Z._%-]+")) {
            throw new VerificationException("email not parse");
        }
    }
}
