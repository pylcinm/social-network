package com.getjavajob.training.web1609.pyltsinm.service.verificator;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.service.exception.VerificationException;

/**
 * Created by Pyltsin on 21.12.2016. Algo8
 */
public interface AbstractChecker {
    void check(Account account) throws VerificationException;
}
