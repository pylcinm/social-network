package com.getjavajob.training.web1609.pyltsinm.service;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.common.Send;
import com.getjavajob.training.web1609.pyltsinm.common.to.SendTo;
import com.getjavajob.training.web1609.pyltsinm.dao.SendDAO;
import com.getjavajob.training.web1609.pyltsinm.dao.interfaces.GenericDAO;
import com.getjavajob.training.web1609.pyltsinm.exception.PersistException;
import com.getjavajob.training.web1609.pyltsinm.service.exception.VerificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pyltsin on 18.08.2017.
 */
@Transactional(readOnly = true)
@Service("sendService")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SendService extends AbstractService<Send> {
    private static Logger logger = LoggerFactory.getLogger(SendService.class);

    @Autowired
    AccountService accountService;
    SendDAO sendDAO;

    @Autowired
    public SendService(@Qualifier("sendDAO") GenericDAO<Send> dao) {
        super(dao);
        this.sendDAO = (SendDAO) dao;
    }

    @Override
    protected void validate(Send send) throws VerificationException {
        return;
    }

    @Transactional
    public void saveMassage(String loginFrom, String loginTo, String message) {
        Account from = accountService.getByName(loginFrom);
        Account to = accountService.getByName(loginTo);
        Send send = new Send(from, to, message);
        try {
            create(send);
        } catch (PersistException | VerificationException e) {
            e.printStackTrace();
            logger.error(e.toString());
        }
    }

    public List<SendTo> getMessages(String loginFrom, String loginTo) {
        Account from = accountService.getByName(loginFrom);
        Account to = accountService.getByName(loginTo);

        List<Send> sends = sendDAO.getSends(from, to, 20);

        List<SendTo> out = new ArrayList<>();
        sends.forEach(send -> out.add(new SendTo(send.getMessage(), send.getFrom().getName(),
                send.getTo().getName(), send.getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))));
        return out;
    }
}
