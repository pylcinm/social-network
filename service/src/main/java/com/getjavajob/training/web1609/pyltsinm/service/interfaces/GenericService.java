package com.getjavajob.training.web1609.pyltsinm.service.interfaces;

import com.getjavajob.training.web1609.pyltsinm.exception.PersistException;
import com.getjavajob.training.web1609.pyltsinm.service.exception.VerificationException;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Pyltsin on 15.01.2017. Algo8
 */
public interface GenericService<T> {
    List<T> getAll() throws PersistException;

    T create(T t) throws PersistException, VerificationException;

    void delete(T t) throws PersistException;

    void edit(T T) throws PersistException, VerificationException;

    T get(int id);

    T getByName(String name);

    void saveImage(T t, InputStream file);

    byte[] getPicture(int id);
}

