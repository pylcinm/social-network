package com.getjavajob.training.web1609.pyltsinm.service.exception;

/**
 * Created by Pyltsin on 21.12.2016. Algo8
 */
public class VerificationException extends Exception {
    public VerificationException(String message) {
        super(message);
    }
}
