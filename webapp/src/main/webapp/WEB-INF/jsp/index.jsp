<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Pyltsin
  Date: 14.01.2017
  Time: 17:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Вход</title>
    <jsp:include page="getCss.jsp"/>
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet">
</head>
<body>

<div class="container">

    <form class="form-signin" name="auth" method="post" action="InServlet">
        <h2 class="form-signin-heading">Войти в сеть</h2>
        <c:if test="${error}">
            <div class="alert alert-danger" role="alert">
                <strong>Ошибка!</strong> Логин/пароль не корректны.
            </div>

        </c:if>


        <label for="inputLogin" class="sr-only">Login</label>
        <input type="text" id="inputLogin" name="login" class="form-control" placeholder="login" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="save" value="yes"> Сохранить данные?
            </label>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <h2><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/register.html">Регистрация</a>
        </h2>
    </form>

</div> <!-- /container -->

<%--<p>Войти в сеть</p>--%>
<%--<form name="auth" method="post" action="InServlet">--%>


<%--<p><b>Логин:</b>--%>
<%--<input type="text" name="login" size="40">--%>
<%--</p>--%>

<%--<p><strong>Пароль:</strong>--%>
<%--<input type="password" name="password" size="40"></p>--%>

<%--<p><input type="checkbox" name="save" value="yes">Сохранить данные?<Br></p>--%>

<%--<p><input type="submit" value="Отправить"></p>--%>
<%--</form>--%>

<%--<p><a href="register.html">Регистрация</a></p>--%>

<jsp:include page="getCssEnd.jsp"/>


</body>
</html>
