package com.getjavajob.training.web1609.pyltsinm.ui;

import com.getjavajob.training.web1609.pyltsinm.help.HelpAuth;
import com.getjavajob.training.web1609.pyltsinm.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Pyltsin on 25.02.2017. Algo8
 */
@Controller
public class EnterController {

    private static Logger logger = LoggerFactory.getLogger(EnterController.class);

    private final AccountService as;

    @Autowired
    public EnterController(AccountService as) {
        this.as = as;
    }

    @RequestMapping(value = {"/", "/index", "IndexServlet"})
    public ModelAndView indexError(@RequestParam(value = "error", required = false) String errorTxt) {
        boolean error = errorTxt != null && errorTxt.equals("true");

        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("error", error);
        return modelAndView;
    }

    @RequestMapping(value = {"/InServlet", "/EnterServlet"})
    public String enter(@RequestParam("login") String login, @RequestParam("password") String password,
                        @RequestParam(value = "save", required = false) String s, HttpServletRequest request, HttpServletResponse response) {
        //чтение логина и пароля
        //Если есть в базе - создание куков и переход на пользователя
        //если нет - возвращаемся обратно.
//        System.out.println("Inservlet");
        if (login == null || login.equals("")) {
            return "redirect: ";
        }
        boolean mustSave = s != null && s.equals("yes");

        return HelpAuth.auth(request, response, as, login, password, mustSave);
    }

    @RequestMapping(value = {"out"})
    public String out(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(HelpAuth.LOGIN) || cookie.getName().equals(HelpAuth.PASSWORD)) {
                    cookie.setMaxAge(0);
                    resp.addCookie(cookie);
                }
            }
        }

        session.invalidate();
        return "redirect: index";
    }
}
