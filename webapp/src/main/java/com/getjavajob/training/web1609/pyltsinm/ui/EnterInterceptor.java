package com.getjavajob.training.web1609.pyltsinm.ui;


import com.getjavajob.training.web1609.pyltsinm.help.HelpAuth;
import com.getjavajob.training.web1609.pyltsinm.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Pyltsin on 06.01.2017. Algo8
 */
public class EnterInterceptor extends HandlerInterceptorAdapter {
    private static Logger logger = LoggerFactory.getLogger(EnterInterceptor.class);
    private final AccountService accountService;

    @Autowired
    public EnterInterceptor(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        String path = request.getRequestURI();

        Set<String> pathOut = new HashSet<>();
        pathOut.add("/register.html");
        pathOut.add("/index");
        pathOut.add("/");
        pathOut.add("/css/signin.css");
        pathOut.add("/InServlet");
        pathOut.add("/RegisterServlet");
        pathOut.add("/HelloWorldFriends");


        String pathIn = HelpAuth.PATH;

        Set<String> pathOut2 = new HashSet<>();
        pathOut2.add(pathIn + "/");
        for (String s : pathOut) {
            pathOut2.add(pathIn + s);
        }

        pathOut.addAll(pathOut2);

        for (String s : pathOut) {
//            System.out.println("EnterInterceptor");
//            System.out.println(s);
//            System.out.println(path);
//            System.out.println((s.trim().toLowerCase()).contains(path.trim().toLowerCase()));
            if ((s.trim().toLowerCase()).contains(path.trim().toLowerCase()) || path.equals("") || path.equals("/")) {
                return super.preHandle(request, response, handler);
            }
            if (path.endsWith(".css")) {
                return super.preHandle(request, response, handler);
            }
        }


        HttpSession session = request.getSession();
        Boolean auth = (Boolean) session.getAttribute(HelpAuth.AUTH);
        if (auth != null && auth) {
            return super.preHandle(request, response, handler);
        } else {
            Cookie[] cookies = request.getCookies();
            String login = null;
            String password = null;
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(HelpAuth.LOGIN)) {
                        login = cookie.getValue();
                    }
                    if (cookie.getName().equals(HelpAuth.PASSWORD)) {
                        password = cookie.getValue();
                    }
                }
            }

            if (login != null && password != null) {
                response.sendRedirect(HelpAuth.auth(request, response, accountService, login, password, true));
                return false;
            } else {

                request.getRequestDispatcher("index").forward(request, response);
                return false;
            }
        }
    }
}
