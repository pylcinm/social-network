package com.getjavajob.training.web1609.pyltsinm.help;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.common.Phone;
import com.getjavajob.training.web1609.pyltsinm.service.AccountService;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class HelpAuth implements Serializable {
    public static final String ACCOUNT = "ACCOUNT";
    public static final String AUTH = "AUTH";
    public static final String LOGIN = "LOGIN";
    public static final String PASSWORD = "PASSWORD";
    public static final String PATH = "/";


    public static String auth(HttpServletRequest request, HttpServletResponse response, AccountService accountService,
                              String login, String password, boolean save) {
//        System.out.println("in auth");

        Account account = getAccount(login, password, accountService);
        if (account != null) {

            doID(request, account);
            if (save) {
                Cookie cookieLogin = new Cookie(LOGIN, login);
                int TIME = 60 * 60;
                cookieLogin.setMaxAge(TIME);
                Cookie cookiePassword = new Cookie(PASSWORD, password);
                cookiePassword.setMaxAge(TIME);

                response.addCookie(cookieLogin);
                response.addCookie(cookiePassword);
            }
            return "redirect: account";
        } else {
            return "redirect: index?error=true";
        }
    }

    private static void doID(HttpServletRequest request, Account account) {
        HttpSession session = request.getSession();
        session.setAttribute(AUTH, true);
        session.setAttribute(ACCOUNT, account);
    }

    private static Account getAccount(String name, String password, AccountService accountService) {
        Account account = accountService.getByName(name);
        if (account == null) {
            return null;
        }
        if (checkPassword(account.getPassword(), password)) {
            return account;
        } else {
            return null;
        }
    }

    private static boolean checkPassword(String hashPassword, String password) {
        return BCrypt.checkpw(password, hashPassword);
    }

    public static Account getAccount(HttpServletRequest request) throws ParseException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String hashPassword = getHashPassword(password);
        String firstName = request.getParameter("firstName");
        String middleName = request.getParameter("middleName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phone1 = request.getParameter("phone1");
        String phone2 = request.getParameter("phone2");
        String date = request.getParameter("birthday");
        Account account = new Account(login, hashPassword);
        account.setFirstName(firstName);
        account.setMiddleName(middleName);
        account.setLastName(lastName);
        account.setEmail(email);
        account.getTelephones().add(new Phone(phone1, ""));
        account.getTelephones().add(new Phone(phone2, ""));
        if (date == null) {
            account.setDate(new SimpleDateFormat("yyyy-mm-DD").parse("1900-00-00"));
        } else {
            try {
                account.setDate(new SimpleDateFormat("yyyy-mm-DD").parse(date));
            } catch (ParseException e) {
                account.setDate(new SimpleDateFormat("yyyy-mm-DD").parse("1900-00-00"));
            }
        }


        return account;
    }

    private static String getHashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }
}