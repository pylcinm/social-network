package com.getjavajob.training.web1609.pyltsinm.ui;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.common.to.SendTo;
import com.getjavajob.training.web1609.pyltsinm.exception.PersistException;
import com.getjavajob.training.web1609.pyltsinm.help.HelpAuth;
import com.getjavajob.training.web1609.pyltsinm.service.AccountService;
import com.getjavajob.training.web1609.pyltsinm.service.SendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Pyltsin on 18.08.2017.
 */
@Controller
public class SendController {
    private static Logger logger = LoggerFactory.getLogger(SendController.class);

    @Autowired
    private AccountService as;

    @Autowired
    private SendService ss;


    @RequestMapping(value = "/sends")
    public ModelAndView showFriends(@SessionAttribute(HelpAuth.ACCOUNT) Account enterAc) {

        Set<? extends Account> friends = new HashSet<>();
        try {
            friends = as.getFriends(enterAc);
        } catch (PersistException e) {
            logger.error("showFriends" + e);

        }


        ModelAndView modelAndView = new ModelAndView("sends");
        modelAndView.addObject("friends", friends);


        return modelAndView;
    }

    @ResponseBody
    @RequestMapping(value = "/sendMessage")
    public String sendMessage(@SessionAttribute(HelpAuth.ACCOUNT) Account enterAc,
                              @RequestParam("loginTo") String loginTo,
                              @RequestParam("message") String message) {

        ss.saveMassage(enterAc.getLogin(), loginTo, message);

        return "";
    }


    @ResponseBody
    @RequestMapping(value = "/getMessages")
    public List<SendTo> getMessages(@SessionAttribute(HelpAuth.ACCOUNT) Account enterAc,
                                    @RequestParam("loginTo") String loginTo) {
        return ss.getMessages(enterAc.getLogin(), loginTo);
    }

}
