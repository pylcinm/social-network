package com.getjavajob.training.web1609.pyltsinm.ui;

import com.getjavajob.training.web1609.pyltsinm.common.Account;
import com.getjavajob.training.web1609.pyltsinm.common.Phone;
import com.getjavajob.training.web1609.pyltsinm.exception.PersistException;
import com.getjavajob.training.web1609.pyltsinm.help.HelpAuth;
import com.getjavajob.training.web1609.pyltsinm.help.HelpUpload;
import com.getjavajob.training.web1609.pyltsinm.service.AccountService;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Pyltsin on 25.02.2017. Algo8
 */

@Controller
public class AccountController {

    private static Logger logger = LoggerFactory.getLogger(AccountController.class);
    private AccountService as;

    public AccountController(AccountService as) {
        this.as = as;
    }

    public AccountService getAs() {
        return as;
    }

    @Autowired
    public void setAs(AccountService as) {
        this.as = as;
    }

    @RequestMapping(value = "/accountChange")
    public ModelAndView accountChange(@SessionAttribute(HelpAuth.ACCOUNT) Account enterAc) {
        System.out.println(enterAc.getDate());
        logger.debug("accountChange");

        return new ModelAndView("accountChange");
    }

    @RequestMapping(value = "/friends")
    public ModelAndView showFriends(@SessionAttribute(HelpAuth.ACCOUNT) Account enterAc) {

        Set<? extends Account> friends = new HashSet<>();
        try {
            friends = as.getFriends(enterAc);
        } catch (PersistException e) {
            logger.error("showFriends" + e);

        }


        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("friends", friends);
        logger.debug("showFriends");

        return modelAndView;
    }

    @RequestMapping(value = "/RegisterServlet", method = RequestMethod.POST)
    public String register(HttpServletRequest request, HttpServletResponse response) {
        Account account = null;
        try {
            account = HelpAuth.getAccount(request);
        } catch (ParseException e) {
            logger.error("register" + e);
        }
        if (as.check(account)) {
            InputStream file = HelpUpload.getStreamFromUpload(request);

            as.createWithPicture(account, file);

            return "redirect: index";

        } else {
            return "redirect: register.html";
        }


    }

    @Transactional
    @RequestMapping(value = "/friends/{type}", method = RequestMethod.GET)
    public String changeFriends(@SessionAttribute(HelpAuth.ACCOUNT) Account accountEnter,
                                @RequestParam("login") String login,
                                @PathVariable(value = "type") String type) {

        accountEnter = as.get(accountEnter.getId());
        if (login == null || login.equals("")) {
            return "redirect: /account";
        }

        if (type.equals("addFriends")) {

            try {
                as.addFriend(accountEnter, login);
            } catch (PersistException e) {
                logger.error("changeFriends" + e);
            }
        } else {
            try {
                as.deleteFriend(accountEnter, login);
            } catch (PersistException e) {
                logger.error("changeFriends" + e);
            }
        }
        if (HelpAuth.PATH != "/") {
            return "redirect: " + HelpAuth.PATH + "/account?login=" + login;
        } else {
            return "redirect: /account?login=" + login;
        }
    }

    private void getAccount(Account accountForEdit, Account account, String date) {
        String password = accountForEdit.getPassword();
        String hashPassword = getHashPassword(password);
        String firstName = accountForEdit.getFirstName();
        String middleName = accountForEdit.getMiddleName();
        String lastName = accountForEdit.getLastName();
        String email = accountForEdit.getEmail();
        if (password != null && !password.equals("")) {
            account.setPassword(hashPassword);
        }
        account.setFirstName(firstName);
        account.setMiddleName(middleName);
        account.setLastName(lastName);
        account.setEmail(email);
        account.clearTelephones();

        for (Phone phone : accountForEdit.getTelephones()) {
            if (phone.getType() != null && phone.getTelephone() != null) {
                account.getTelephones().add(phone);
            }
        }

        try {
            account.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        } catch (ParseException e) {
            logger.error("getAccount" + e);
        }
    }

    @Transactional
    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ModelAndView showAccount(@SessionAttribute(HelpAuth.ACCOUNT) Account accountEnter,
                                    @SessionAttribute(HelpAuth.ACCOUNT) Account account,
                                    @RequestParam(value = "login", required = false) String login) {

        ModelAndView modelAndView = new ModelAndView("account");

        boolean accountEquals = login == null || login.equals("");

        if (login != null && login.equals(account.getLogin())) {
            accountEquals = true;
        }


        account = accountEquals ? as.get(account.getId()) : as.getByName(login);


        boolean accountFind = account != null;

        if (!accountFind) {
            accountEquals = true;
            account = accountEnter;
        }

        boolean isFriends = false;

        accountEnter = as.get(accountEnter.getId());
        if (!accountEquals) {

            isFriends = as.isFriends(accountEnter, account);
        }

        modelAndView.addObject("account", account);
        modelAndView.addObject("accountEquals", accountEquals);
        modelAndView.addObject("isFriends", isFriends);
        return modelAndView;
    }

    @Transactional
    @RequestMapping(value = {"/ChangeServlet"}, method = RequestMethod.POST)
    public String changeAccount(@ModelAttribute(name = "account") Account accountForEdit,
//                                @RequestParam(value = "telephones.telephone", required = false) String[] phoneN,
//                                @RequestParam(value = "telephones.type", required = false) String[] phoneT,
                                @RequestParam("foto") MultipartFile mFile,
                                @RequestParam("date") String date,
                                @SessionAttribute(HelpAuth.ACCOUNT) Account accountEnter, HttpSession session) {
//        if (phoneN == null) {
//            phoneN = new String[]{};
//        }
//
//        if (phoneT == null) {
//            phoneT = new String[]{};
//        }

        accountEnter = as.get(accountEnter.getId());

        getAccount(accountForEdit, accountEnter, date);
        InputStream fileFromSpring = null;
        try {
            fileFromSpring = mFile.getInputStream();
        } catch (IOException e) {
            logger.error("changeAccount" + e);
        }

        try {
            as.editWithPicture(accountEnter, fileFromSpring);
            session.setAttribute(HelpAuth.ACCOUNT, accountEnter);
        } catch (IOException e) {
            logger.error("changeAccount" + e);
        }

        return "redirect: account";
    }


    @Transactional
    @RequestMapping(value = "/createXML", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response, @SessionAttribute(HelpAuth.ACCOUNT) Account accountEnter) throws IOException {

        Account account = as.get(accountEnter.getId());
        //create XML string

        StringWriter writer = new StringWriter();

        // Marshaller
        try {
            JAXBContext context = JAXBContext.newInstance(Phone.class, Account.class);

            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(account, writer);

        } catch (JAXBException e) {
            logger.error(e.toString());
        }


        String mimeType = "application/xml";

        response.setContentType(mimeType);

        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + "account.xml");

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                response.getOutputStream(), "UTF-8"))
        ) {
            bw.write(writer.toString());

        }
    }


    @RequestMapping(value = {"/importXML"})
    public ModelAndView importXML(HttpServletRequest req, @RequestParam("fileXML") MultipartFile mFil,
                                  @SessionAttribute(HelpAuth.ACCOUNT) Account accountEnter) throws ServletException, IOException {


        Account account = null;
        try {
            JAXBContext context = JAXBContext.newInstance(Phone.class, Account.class);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            account = (Account) unmarshaller.unmarshal(mFil.getInputStream());


        } catch (JAXBException e) {
            logger.error(e.toString());
        }

        if (account != null) {
            accountEnter = as.get(accountEnter.getId());

            getAccountXML(account, accountEnter);
        }

        try {
            as.editWithPicture(accountEnter, null);
            req.getSession().setAttribute(HelpAuth.ACCOUNT, accountEnter);
        } catch (IOException e) {
            logger.error("changeAccount" + e);
        }

        return new ModelAndView("redirect: /account");
    }

    private void getAccountXML(Account account, Account accountEnter) {
        accountEnter.setFirstName(account.getFirstName());
        accountEnter.setMiddleName(account.getMiddleName());
        accountEnter.setLastName(account.getLastName());
        accountEnter.setDate(account.getDate());
        accountEnter.setEmail(account.getEmail());
        accountEnter.setTelephones(account.getTelephones());
    }

    private String getHashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

}
