package com.getjavajob.training.web1609.pyltsinm.common.interfaces;

/**
 * Created by Pyltsin on 05.12.2016. Algo8
 */
public interface Identified {
    /**
     * @return ID
     */
    int getId();
}
