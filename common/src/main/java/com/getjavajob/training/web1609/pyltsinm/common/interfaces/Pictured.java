package com.getjavajob.training.web1609.pyltsinm.common.interfaces;

/**
 * Created by Pyltsin on 12.03.2017. Algo8
 */
public interface Pictured {
    byte[] getPicture();

    void setPicture(byte[] picture);
}
